using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    //カウントアップ
    private float countup = 0.0f;

    public GameObject[] TimerPrefab_0001;
    public GameObject[] TimerPrefab_0010;
    public GameObject[] TimerPrefab_0100;
    public GameObject[] TimerPrefab_1000;

    float dt0001 = 0;
    float dt0010 = 0;
    float dt0100 = 0;
    float dt1000 = 0;

    int count_0001 = 0;
    int count_0010 = 0;
    int count_0100 = 0;
    int count_1000 = 0;

    // Update is called once per frame
    void Update()
    {
        //時間をカウントする
        countup += Time.deltaTime;

        dt0001 += Time.deltaTime;
        dt0010 += Time.deltaTime;
        dt0100 += Time.deltaTime;
        dt1000 += Time.deltaTime;

        Debug.Log(countup);

        if (dt0001 > 1)//1秒ごとに実行
        {
            TimerPrefab_0001[count_0001].SetActive(false);//非表示
            count_0001++;
            dt0001 = 0.0f;

            if(count_0001 == 10)
            {
                count_0001 = 0;
            }

            TimerPrefab_0001[count_0001].SetActive(true);//非表示

        }

        if (dt0010 > 10)//10秒ごとに実行
        {
            TimerPrefab_0010[count_0010].SetActive(false);//非表示
            count_0010++;
            dt0010 = 0.0f;

            if (count_0010 == 10)
            {
                count_0010 = 0;
            }

            TimerPrefab_0010[count_0010].SetActive(true);//非表示
        }

        if (dt0100 > 60)//60秒ごとに実行
        {
            TimerPrefab_0100[count_0100].SetActive(false);//非表示
            count_0100++;
            dt0100 = 0.0f;

            if (count_0100 == 6)
            {
                count_0100 = 0;
            }

            TimerPrefab_0100[count_0100].SetActive(true);//非表示
        }

        if (dt1000 > 600)//600秒ごとに実行
        {
            TimerPrefab_1000[count_1000].SetActive(false);//非表示
            count_1000++;
            dt1000 = 0.0f;

            if (count_1000 == 6)
            {
                count_1000 = 0;
            }

            TimerPrefab_1000[count_1000].SetActive(true);//非表示
        }

    }
}
