using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Rogo : MonoBehaviour
{
    public GameObject FeedIn;   //フェードインプレハブ
    public GameObject FeedOut;  //フェードアウトプレハブ

    // Start is called before the first frame update
    void Start()
    {
        Invoke("LoadEndingSceneTitle", 4.5f); //4.5秒後にタイトルシーンへ移行

        Invoke("FeedI", 1.0f);
        Invoke("FeedO", 3.0f);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void FeedI()
    {
        FeedIn.SetActive(false);
    }

    public void FeedO()
    {
        FeedOut.SetActive(true);
    }

    public void LoadEndingSceneTitle()
    {
        SceneManager.LoadScene("TitlScene");//タイトルシーンへ移行
    }
}
