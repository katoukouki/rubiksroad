using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickToZoom : MonoBehaviour
{
    //===== 定義領域 =====
    private Animator anim;  //Animatorをanimという変数で定義する

    //===== 初期処理 =====
    void Start()
    {
        //変数animに、Animatorコンポーネントを設定する
        anim = gameObject.GetComponent<Animator>();
    }

    //===== 主処理 =====
    void Update()
    {
        ////もし、スペースキーが押されたらなら
        //if (Input.GetMouseButtonDown(0))
        //{
        //    //Bool型のパラメーターであるblRotをTrueにする
        //    //Debug.Log("ズームされた");
        //    anim.SetBool("ClicktoZoom", true);
        //}
    }
    public void ToZoom()
    {
        anim.SetBool("Zoom", true);
        TitlManager.instance.TitlButton();
    }
}
