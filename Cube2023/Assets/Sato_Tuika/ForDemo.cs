using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ForDemo : MonoBehaviour
{
    public GameObject FeedOut;
    public GameObject feedin;
    float nexttime;

    //デモムービーが流れるまでの時間（デフォルト30秒）
    float fordemotime =30.0f;

    private void Start()
    {
        //フェードインできる＆フェードアウトしてない時
        if (DemoDirector.iffeedin&&TitlManager.isfeedout==false)
        {
            //フェードインする
            DemoDirector.iffeedin = false;
            //フェードアウトできなくする
            TitlManager.isfeedout = true;
            Feedin();
            Invoke("FeedinOff",0.5f);
        }
    }
    void Update()
    {
        nexttime += Time.deltaTime;

        if (nexttime >= fordemotime)
        {
            nexttime = 0;
            TitleDemo();
        }
        //Debug.Log(nexttime);//何秒経過したか表示する
    }

    public void TitleDemo()
    {
        Invoke("LoadEndingSceneDemo", 1.5f);
        FeedOut.SetActive(true);

    }
    public void LoadEndingSceneDemo()
    {
        SceneManager.LoadScene("Titl_DemoMovie");
    }
    //フェードインする
    void Feedin()
    {
        feedin.SetActive(true);
    }
    //フェードインができなくなる
    void FeedinOff()
    {
        feedin.SetActive(false);
    }
}
