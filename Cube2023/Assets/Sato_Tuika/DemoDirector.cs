using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DemoDirector : MonoBehaviour
{
    public GameObject FeedOut;
    public static bool iffeedin =false;//フェードインできるフラグ

    void Start()
    {

    }

    void Update()
    {
        if(Input.GetMouseButtonDown(0)||Input.GetMouseButtonDown(1))
        {
            iffeedin = true;
            Invoke("LoadEndingSceneDemo", 1.5f);
            FeedOut.SetActive(true);

        }
    }
    public void LoadEndingSceneDemo()
    {
        SceneManager.LoadScene("TitlScene");
    }
}
