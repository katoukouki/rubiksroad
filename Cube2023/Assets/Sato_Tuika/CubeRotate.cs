using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeRotate : MonoBehaviour
{
    public static CubeRotate instance;
    public GameObject RubikCube;

    public GameObject cloneEffect;

    public void Awake()
    {
        //    インスタンスの中身を自分にする
        if (instance == null)
        {
            instance = this;
        }
    }
    void Start()
    {

    }

    void Update()
    {
        if(cloneEffect == null)
        {
            cloneEffect = GameObject.Find("Efects(Clone)");
            Debug.Log("エフェクトを登録した");
        }
    }
    public void CubeRotation()
    {
        RubikCube.transform.Rotate(0.05f, 0.05f, -0.1f);
        cloneEffect.transform.Rotate(0.05f, 0.05f, -0.1f);
    }
}
