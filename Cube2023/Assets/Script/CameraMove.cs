using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class CameraMove : MonoBehaviour
{
    float prevX;    //前のX
    float prevY;    //前のY

    public GameObject mainCamera;

    public GameObject Manager;
    bool getManager;

    public GameObject SettingT;
    public GameObject SettingF;

    public bool getClear;
    public bool getSet;

    // Start is called before the first frame update
    void Start()
    {
        float prevX = Input.mousePosition.x;　//マウスの位置
        float prevY = Input.mousePosition.y;

        Manager = GameObject.Find("CubeManager");
        getManager = Manager.GetComponent<CubeManager>().isMove;　//CubeManagerのisMoobを取得
        getClear = Manager.GetComponent<CubeManager>().isClear;

        //SettingT = GameObject.Find("Seeting(true)");
        //SettingF = GameObject.Find("Seeting(false)");
    }

    public void SetT()
    {
        getSet = SettingT.GetComponent<SettingButton>().CameraMove;
    }

    public void SetF()
    {
        getSet = SettingF.GetComponent<SettingButton>().CameraMove;
    }

    // Update is called once per frame
    void Update()
    {
        if (getManager == false)　//動けない
        {
            return;
        }

        float nowX = Input.mousePosition.x; //今のX
        float nowY = Input.mousePosition.y;

        if(getClear == false && getSet == false)
        {
            //マウスボタンが押されている（左クリック）
            if (Input.GetMouseButton(1))
            {
                if (Manager.GetComponent<CubeManager>().isHitObject == true)
                {
                    return;
                }
                transform.Rotate(new Vector3(-(nowY - prevY) / 2.0f, (nowX - prevX) / 2.0f, 0));
            }

            prevX = nowX; //上書き
            prevY = nowY;
        }

        if(getClear == true)
        {
            transform.Rotate(new Vector3(0.05f, 0.05f, -0.1f));
        }


        //transform.Rotate(0, 0, 360 / 50 * Time.deltaTime);

    }

}
