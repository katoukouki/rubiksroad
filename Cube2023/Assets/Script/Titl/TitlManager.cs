using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TitlManager : MonoBehaviour
{

    public GameObject FeedOut;

    //グローバル関数（ほかのスクリプトでも使えるようになる）
    public static TitlManager instance;//グローバル関数
    public static bool isfeedout;//グローバル変数

    //グローバル関数の初期化
    public void Awake()
    {
        //    インスタンスの中身を自分にする
        if (instance == null)
        {
            instance = this;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void TitlButton()
    {
        //効果音再生 0.3秒後にPlaySEメソッドを呼ぶ
        Invoke("PlaySE", 0);
        //Invoke("LoadEndingSceneStageSelect", 1.5f);
        //FeedOut.SetActive(true);

        //フェードアウト関数を呼び出す
        Invoke("ForFeedOut", 2.5f);
        //ステージセレクトシーンに飛ぶ
        Invoke("LoadEndingSceneStageSelect", 4.5f);
    }

    public void CreditButton()
    {
        //効果音再生 0.3秒後にPlaySEメソッドを呼ぶ
        Invoke("PlaySE", 0);
        Invoke("LoadEndingSceneCLEDIT", 1.5f);
        //FeedOut.SetActive(true);
        //フェードアウトする
        ForFeedOut();
    }
    void LoadEndingSceneStageSelect()
    {
        SceneManager.LoadScene("StageSelectScene");
    }

    void LoadEndingSceneCLEDIT()
    {
        SceneManager.LoadScene("CLEDIT");
    }

    void PlaySE()
    {
        //効果音再生
        GetComponent<AudioSource>().Play();
    }

    //フェードアウト関数
    void ForFeedOut()
    {
        FeedOut.SetActive(true);//フェードアウトする
        isfeedout = true;//フェードインできなくなる
    }
}
