using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SettingButton : MonoBehaviour
{
    public GameObject RetryUI;
    public GameObject RetireUI;
    public GameObject SetUIf;
    public GameObject SetUIt;

    public GameObject CubeManajer;
    public GameObject Camera;

    public bool CameraMove = true;
    public bool CubeMove = true;

    public GameObject FeedOut;

    // Start is called before the first frame update
    void Start()
    {
        //Camera = GameObject.Find("Center");
        //CubeManajer = GameObject.Find("CubeManager");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetButtonfalse()
    {
        gameObject.SetActive(false);
        SetUIf.SetActive(true);
        //RetryUI.SetActive(false);
        //RetireUI.SetActive(false);
        CameraMove = false;
        CubeMove = false;
        Camera.GetComponent<CameraMove>().SetT();
        CubeManajer.GetComponent<CubeManager>().SetT();
    }
public void SetButtontrue()
    {
        gameObject.SetActive(false);
        SetUIt.SetActive(true);
        //RetryUI.SetActive(false);
        //RetireUI.SetActive(false);
        CameraMove = true;
        CubeMove = true;
        Camera.GetComponent<CameraMove>().SetF();
        CubeManajer.GetComponent<CubeManager>().SetF();
    }

    public void RetireButton()
    {
        //効果音再生 0.3秒後にPlaySEメソッドを呼ぶ
        Invoke("PlaySE", 0);
        Invoke("LoadEndingSceneStageSelect", 1.5f);
        FeedOut.SetActive(true);
    }

    public void RetryButton()
    {
        //効果音再生 0.3秒後にPlaySEメソッドを呼ぶ
        Invoke("PlaySE", 0);
        Invoke("LoadEndingSceneRetry", 1.5f);
        FeedOut.SetActive(true);
    }

    void PlaySE()
    {
        //効果音再生
        GetComponent<AudioSource>().Play();
    }

    void LoadEndingSceneStageSelect()
    {
        SceneManager.LoadScene("StageSelectScene");
    }

    void LoadEndingSceneRetry()
    {
        Scene loadScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(loadScene.name);
    }
}
