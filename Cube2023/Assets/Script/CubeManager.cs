using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CubeManager : MonoBehaviour
{
    public GameObject[,,] cube = new GameObject[3, 3, 3];

    Vector3 anchorPoint;    //回転の中心位置
    Vector3 rotateAxis;     //回転軸
    float angle = 0.0f;    //何度回転したか（90度になったら止める
    public bool isRotating = false;  //回転アニメーション中かどうか（回転中は操作不能）
    GameObject[] rotateCube = new GameObject[9];    //回転させるキューブたち（9個）
    public bool isCanRotation = true;  //回転できるかどうか

    GameObject cameraMove;

    public Camera camera_object; //カメラを取得
    private RaycastHit hit; //レイキャストが当たったものを取得する入れ物

    public bool isMove = true;　//動けるか
    public bool isHitObject = false;//オブジェクトに触れているか

    public bool isClear = false;//クリアしているかどうか
    public bool getSet;

    public GameObject SettingT;
    public GameObject SettingF;

    // public GameObject GreenQuad;
    public List<GameObject> StartQuad = new List<GameObject>();
    public List<GameObject> GoalQuad = new List<GameObject>();

    //public GameObject StartQuad;
    //public GameObject GoalQuad;
    public GameObject cubes;
    public GameObject Efects; //エフェクトオブジェクト
    //public GameObject Efects2; //エフェクトオブジェクト

    Vector3 touchPoint; //操作するために触った位置

    public float rotateSpeed;

    //ドラッグした方向
    enum DragDir
    {
        NONE,       //無し
        POSI_X,     //X軸でプラス方向
        NEGA_X,     //X軸でマイナス方向
        POSI_Y,
        NEGA_Y,
        POSI_Z,
        NEGA_Z
    };
    DragDir dragDir = DragDir.NONE;
    int row;   //回転させる列



    // Start is called before the first frame update
    void Start()
    {
        //director = GameObject.Find("GameDirector");
        cameraMove = GameObject.Find("Center");
    }

    public void SetT()
    {
        getSet = SettingT.GetComponent<SettingButton>().CameraMove;
    }

    public void SetF()
    {
        getSet = SettingF.GetComponent<SettingButton>().CameraMove;
    }

    // Update is called once per frame
    void Update()
    {
        if (isClear == false && getSet == false)
        {

            if (UnityEngine.Input.GetMouseButtonUp(0))
            {
                isCanRotation = true;   //回転可能
            }

            if (UnityEngine.Input.GetKeyDown(KeyCode.Space))
            {
                GoalCheck();
            }


            //回転中
            if (isRotating)
            {
                //回転アニメーション
                Rotate();

            }

            //停止中
            else
            {
                //操作待ち
                Input();
            }

            if (isCanRotation == true)
            {
                if (UnityEngine.Input.GetMouseButtonDown(0))
                {
                    Ray ray = camera_object.ScreenPointToRay(UnityEngine.Input.mousePosition); //マウスのポジションを取得してRayに代入

                    if (Physics.Raycast(ray, out hit))  //マウスのポジションからRayを投げて何かに当たったらhitに入れる
                    {
                        isHitObject = true;
                        touchPoint = hit.point;
                    }
                }

                if (UnityEngine.Input.GetMouseButton(0)) //マウスが左クリックされている間
                {
                    Ray ray = camera_object.ScreenPointToRay(UnityEngine.Input.mousePosition); //マウスのポジションを取得してRayに代入

                    if (Physics.Raycast(ray, out hit))  //マウスのポジションからRayを投げて何かに当たったらhitに入れる
                    {
                        //今触った
                        if (!isHitObject)
                        {
                            touchPoint = hit.point;

                        }

                        //さっきから触ってる（ドラッグ中）
                        else
                        {
                            Vector3 drag = hit.point - touchPoint;

                            //マウス動いてない
                            if (drag.magnitude < 0.03f)
                            {
                                dragDir = DragDir.NONE;
                                return;
                            }

                            //X方向にドラッグしてる
                            if (Mathf.Abs(drag.x) > Mathf.Abs(drag.y) && Mathf.Abs(drag.x) > Mathf.Abs(drag.z))
                            {
                                if (touchPoint.z <= -0.4f)
                                {
                                    if (drag.x > 0)
                                        dragDir = DragDir.POSI_Y;
                                    else
                                        dragDir = DragDir.NEGA_Y;
                                    row = (int)(touchPoint.y + 0.5f);
                                }
                                else if (touchPoint.z >= 2.4f)
                                {
                                    if (drag.x > 0)
                                        dragDir = DragDir.NEGA_Y;
                                    else
                                        dragDir = DragDir.POSI_Y;
                                    row = (int)(touchPoint.y + 0.5f);
                                }
                                else if (touchPoint.y >= 2.4)
                                {
                                    if (drag.x > 0)
                                        dragDir = DragDir.POSI_Z;
                                    else
                                        dragDir = DragDir.NEGA_Z;
                                    row = (int)(touchPoint.z + 0.5f);
                                }
                                else
                                {
                                    if (drag.x > 0)
                                        dragDir = DragDir.NEGA_Z;
                                    else
                                        dragDir = DragDir.POSI_Z;
                                    row = (int)(touchPoint.z + 0.5f);
                                }
                            }


                            //Y方向にドラッグしてる
                            else if (Mathf.Abs(drag.y) > Mathf.Abs(drag.x) && Mathf.Abs(drag.y) > Mathf.Abs(drag.z))
                            {
                                if (touchPoint.x <= -0.4)
                                {
                                    if (drag.y > 0)
                                        dragDir = DragDir.POSI_Z;
                                    else
                                        dragDir = DragDir.NEGA_Z;
                                    row = (int)(touchPoint.z + 0.5f);
                                }
                                else if (touchPoint.x >= 2.4)
                                {
                                    if (drag.y > 0)
                                        dragDir = DragDir.NEGA_Z;
                                    else
                                        dragDir = DragDir.POSI_Z;
                                    row = (int)(touchPoint.z + 0.5f);
                                }
                                else if (touchPoint.z >= 2.4)
                                {
                                    if (drag.y > 0)
                                        dragDir = DragDir.POSI_X;
                                    else
                                        dragDir = DragDir.NEGA_X;
                                    row = (int)(touchPoint.x + 0.5f);
                                }
                                else
                                {
                                    if (drag.y > 0)
                                        dragDir = DragDir.NEGA_X;
                                    else
                                        dragDir = DragDir.POSI_X;
                                    row = (int)(touchPoint.x + 0.5f);
                                }
                            }

                            //Z方向にドラッグしてる
                            else
                            {
                                if (touchPoint.y <= -0.4f)
                                {
                                    if (drag.z > 0)
                                        dragDir = DragDir.POSI_X;
                                    else
                                        dragDir = DragDir.NEGA_X;
                                    row = (int)(touchPoint.x + 0.5f);
                                }
                                else if (touchPoint.y >= 2.4f)
                                {
                                    if (drag.z > 0)
                                        dragDir = DragDir.NEGA_X;
                                    else
                                        dragDir = DragDir.POSI_X;
                                    row = (int)(touchPoint.x + 0.5f);
                                }
                                else if (touchPoint.x >= 2.4)
                                {
                                    if (drag.z > 0)
                                        dragDir = DragDir.POSI_Y;
                                    else
                                        dragDir = DragDir.NEGA_Y;
                                    row = (int)(touchPoint.y + 0.5f);
                                }
                                else
                                {
                                    if (drag.z > 0)
                                        dragDir = DragDir.NEGA_Y;
                                    else
                                        dragDir = DragDir.POSI_Y;
                                    row = (int)(touchPoint.y + 0.5f);
                                }
                            }
                            isCanRotation = false;  //1回転したら90度で止まる
                        }



                        //Debug.Log(hit.point);
                        isMove = false;
                    }
                }
                else
                {
                    isHitObject = false;
                }
            }
        }


    }

    //操作待ち
    private void Input()
    {
        switch (dragDir)
        {
            //ドラッグしてない
            case DragDir.NONE:
                return;

            case DragDir.POSI_X:
                {
                    int x = row;

                    //回転する9個のキューブを選別
                    int i = 0;
                    for (int y = 0; y < 3; y++)
                    {
                        for (int z = 0; z < 3; z++)
                        {

                            rotateCube[i] = cube[x, y, z];
                            i++;
                        }
                    }

                    //回転の中心位置
                    anchorPoint = new Vector3(x, 1, 1);

                    //回転軸
                    rotateAxis = Vector3.left;
                }
                break;


            case DragDir.NEGA_X:
                {
                    int x = row;

                    //回転する9個のキューブを選別
                    int i = 0;
                    for (int y = 0; y < 3; y++)
                    {
                        for (int z = 0; z < 3; z++)
                        {

                            rotateCube[i] = cube[x, y, z];
                            i++;
                        }
                    }

                    //回転の中心位置
                    anchorPoint = new Vector3(x, 1, 1);

                    //回転軸
                    rotateAxis = Vector3.right;
                }
                break;

            case DragDir.POSI_Y:
                {
                    int y = row;

                    //回転する9個のキューブを選別
                    int i = 0;
                    for (int x = 0; x < 3; x++)
                    {
                        for (int z = 0; z < 3; z++)
                        {

                            rotateCube[i] = cube[x, y, z];
                            i++;
                        }
                    }

                    //回転の中心位置
                    anchorPoint = new Vector3(1, y, 1);

                    //回転軸
                    rotateAxis = Vector3.down;
                }
                break;

            case DragDir.NEGA_Y:
                {
                    int y = row;

                    //回転する9個のキューブを選別
                    int i = 0;
                    for (int x = 0; x < 3; x++)
                    {
                        for (int z = 0; z < 3; z++)
                        {

                            rotateCube[i] = cube[x, y, z];
                            i++;
                        }
                    }

                    //回転の中心位置
                    anchorPoint = new Vector3(1, y, 1);

                    //回転軸
                    rotateAxis = Vector3.up;
                }
                break;

            case DragDir.POSI_Z:
                {
                    int z = row;

                    //回転する9個のキューブを選別
                    int i = 0;
                    for (int x = 0; x < 3; x++)
                    {
                        for (int y = 0; y < 3; y++)
                        {

                            rotateCube[i] = cube[x, y, z];
                            i++;
                        }
                    }

                    //回転の中心位置
                    anchorPoint = new Vector3(1, 1, z);

                    //回転軸
                    rotateAxis = Vector3.back;
                }
                break;

            case DragDir.NEGA_Z:
                {
                    int z = row;

                    //回転する9個のキューブを選別
                    int i = 0;
                    for (int x = 0; x < 3; x++)
                    {
                        for (int y = 0; y < 3; y++)
                        {

                            rotateCube[i] = cube[x, y, z];
                            i++;
                        }
                    }

                    //回転の中心位置
                    anchorPoint = new Vector3(1, 1, z);

                    //回転軸
                    rotateAxis = Vector3.forward;
                }
                break;

        }

        //回転スタート
        angle = 0.0f;
        isRotating = true;


        //ディレクターのLimitメソッドを呼び出す

        //効果音再生 0.3秒後にPlaySEメソッドを呼ぶ
        Invoke("PlaySE", 0.3f);
    }



    //回転アニメーション
    void Rotate()
    {
        float a = rotateSpeed * Time.deltaTime;
       // Debug.Log(a);

        //if (a > 5) a = 5;

        //9個のキューブを回転させる
        for (int i = 0; i < 9; i++)
        {
            rotateCube[i].transform.RotateAround(anchorPoint, rotateAxis, a);
        }

        angle += a;

        //90°回った
        if (angle >= 90)
        {
            for (int i = 0; i < 9; i++)
            {
                //回転後の位置を正しい配列の位置にセット
                rotateCube[i].GetComponent<Cube>().ArraySet();
            }

            //停止
            isRotating = false;
            dragDir = DragDir.NONE;

            //0.1秒後にクリア判定（即判定するとうまくいかないときがある）
            Invoke("GoalCheck", 0.1f) ;
        }

        //else
        //{
        //    //まだ回る
        //    angle += 0.5f;
        //}
    }

    //クリアしてないかチェック
    void GoalCheck()
    {

        for (int k = 0; k < StartQuad.Count; k++)
        {
            //チェックフラグリセット
            for (int j = 0; j < cubes.transform.childCount; j++)
            {
                for (int i = 0; i < cubes.transform.GetChild(j).transform.childCount; i++)
                {
                    cubes.transform.GetChild(j).transform.GetChild(i).GetComponent<Quad>().isChecked = false;

                }
            }

            bool result = StartQuad[k].GetComponent<Quad>().ConnectCheck(StartQuad[k].GetComponent<Quad>().quadColor);

            if(!result)
            {
                return;
            }
        }


        Debug.Log("ごーーーーーーる");
        Invoke("LoadEndingSceneClear", 0.0f);
        isClear = true;
        cameraMove.GetComponent<CameraMove>().getClear = true;
        //Instantiate(Efects2);
        //Efects2.transform.position = new Vector3(1, 1, 1);//この数値に出現
        Instantiate(Efects);
        Efects.transform.position = new Vector3(1, 1, 1);//この数値に出現
    }

    public void LoadEndingSceneClear ()
    {
        SceneManager.LoadScene("ClearScene", LoadSceneMode.Additive);
    }

    void PlaySE()
    {
        //効果音再生
        GetComponent<AudioSource>().Play();
    }
}
