using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour
{
    public GameObject connect = null;

    void OnTriggerEnter(Collider other)
    {

        //つながってるクアッド
        if (other.gameObject.transform.parent.tag == "Green")
        {
            if (this.transform.parent.tag == other.transform.parent.tag)
            {
                //if ((this.transform.parent.GetComponent<Quad>().quadColor == other.transform.parent.GetComponent<Quad>().quadColor) ||
                //    this.transform.parent.GetComponent<Quad>().quadColor == Quad.QuadColor.JOKER || other.transform.parent.GetComponent<Quad>().quadColor == Quad.QuadColor.JOKER)
                //{
                    connect = other.gameObject;
                    //Debug.Log("ヒット" + this.transform.parent.parent + "と"+ other.transform.parent.parent);

                //}
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (this.transform.parent.tag == other.transform.parent.tag)
        {
            connect = null;
            //Debug.Log("離れた" + this.transform.parent.parent + "と" + other.transform.parent.parent);
        }
    }
}
