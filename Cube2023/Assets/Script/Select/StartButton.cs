using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class StartButton : MonoBehaviour
{
    public GameObject SelectManager;
    public int getSelectNumber;

    public GameObject FeedOut;

    // Start is called before the first frame update
    void Start()
    {
        SelectManager = GameObject.Find("SelectManager");
        //getSelectNumber = SelectManager.GetComponent<SelectManager>().SelectNumber;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SceneRoad()
    {
        //効果音再生 0.3秒後にPlaySEメソッドを呼ぶ
        Invoke("PlaySE", 0);
        Invoke("SButton", 1.5f);
        FeedOut.SetActive(true);
    }

    void PlaySE()
    {
        //効果音再生
        GetComponent<AudioSource>().Play();
    }

    public void SButton()
    {
        getSelectNumber = SelectManager.GetComponent<SelectManager>().SelectNumber;


        switch (getSelectNumber)
        {
            case 0:
                SceneManager.LoadScene("Stage1Scene");
                break;

            case 1:
                SceneManager.LoadScene("Stage2Scene");
                break;

            case 2:
                SceneManager.LoadScene("Stage3Scene");
                break;

            case 3:
                SceneManager.LoadScene("Stage4Scene");
                break;

            case 4:
                SceneManager.LoadScene("Stage5Scene");
                break;

            case 5:
                SceneManager.LoadScene("Stage6Scene");
                break;

            case 6:
                SceneManager.LoadScene("Stage7Scene");
                break;

            case 7:
                SceneManager.LoadScene("Stage8Scene");
                break;

            case 8:
                SceneManager.LoadScene("Stage9Scene");
                break;

            case 9:
                SceneManager.LoadScene("Stage10Scene");
                break;

            case 10:
                SceneManager.LoadScene("Stage11Scene");
                break;

            case 11:
                SceneManager.LoadScene("Stage12Scene");
                break;

            case 12:
                SceneManager.LoadScene("Stage13Scene");
                break;

            case 13:
                SceneManager.LoadScene("Stage14Scene");
                break;

            case 14:
                SceneManager.LoadScene("Stage15Scene");
                break;

            case 15:
                SceneManager.LoadScene("Stage16Scene");
                break;

            case 16:
                SceneManager.LoadScene("Stage17Scene");
                break;

            case 17:
                SceneManager.LoadScene("Stage18Scene");
                break;

            case 18:
                SceneManager.LoadScene("Stage19Scene");
                break;

            case 19:
                SceneManager.LoadScene("Stage20Scene");
                break;
        }
        Global_Variable.StageNumMemo = getSelectNumber;
        //Debug.Log("StageNumMemo" + Global_Variable.StageNumMemo);
    }


}
