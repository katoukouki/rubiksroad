using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SelectManager : MonoBehaviour
{
	public int SelectNumber = 0; //ステージの番号
	int SelectNumMemory;

	public GameObject getStageCube;
	public GameObject[] LevelUI;

	public GameObject FeedOut;

	// Start is called before the first frame update
	void Start()
    {
        getStageCube = GameObject.Find("StageCube");
        //getStageCube.GetComponent<StageCube>().CubeSpone();

        //追加したプログラム
        if (Global_Variable.StageNumMemo != 0)
        {
            SelectNumMemory = Global_Variable.StageNumMemo;
        }
        if (SelectNumMemory != 0)
        {
            //ルービックキューブを非表示にする
            getStageCube.GetComponent<StageCube>().CubePrefab[SelectNumber].SetActive(false);
            //レベルのUIを非表示にする
            LevelUI[SelectNumber].SetActive(false);


            //最後に選んだステージに対応する見た目に変更する
            getStageCube.GetComponent<StageCube>().CubePrefab[SelectNumMemory].SetActive(true);
            LevelUI[SelectNumMemory].SetActive(true);
        }

        //ステージナンバーを最後に遊んだステージナンバーにする
        SelectNumber = SelectNumMemory;
    }

    // Update is called once per frame
    void Update()
    {

	}

	public void ButtonPushedLeft()
	{
		getStageCube.GetComponent<StageCube>().CubePrefab[SelectNumber].SetActive(false);//非表示
		LevelUI[SelectNumber].SetActive(false);//非表示
		if (SelectNumber == 0)
		{
			SelectNumber = 19;
		}
		else
		{ SelectNumber--; }
		//Debug.Log(SelectNumber);
		StageSelect();
		//getStageCube.GetComponent<StageCube>().CubeSpone();
		getStageCube.GetComponent<StageCube>().CubePrefab[SelectNumber].SetActive(true);//表示
		LevelUI[SelectNumber].SetActive(true);//非表示
		//効果音再生 0.3秒後にPlaySEメソッドを呼ぶ
		Invoke("PlaySE", 0);
	}

	public void ButtonPushedRight()
	{
		getStageCube.GetComponent<StageCube>().CubePrefab[SelectNumber].SetActive(false);
		LevelUI[SelectNumber].SetActive(false);//非表示
		if (SelectNumber == 19)
		{
			SelectNumber = 0;
		}
		else
		{ SelectNumber++; }
		//Debug.Log(SelectNumber);
		StageSelect();
		//getStageCube.GetComponent<StageCube>().CubeSpone();
		getStageCube.GetComponent<StageCube>().CubePrefab[SelectNumber].SetActive(true);
		LevelUI[SelectNumber].SetActive(true);//表示
		//効果音再生 0.3秒後にPlaySEメソッドを呼ぶ
		Invoke("PlaySE", 0);
	}

	public void BackTitl()
    {
		//効果音再生 0.3秒後にPlaySEメソッドを呼ぶ
		Invoke("PlaySE",0);
		Invoke("LoadEndingSceneTitl", 1.5f);
		FeedOut.SetActive(true);
	}

	void PlaySE()
	{
		//効果音再生
		GetComponent<AudioSource>().Play();
	}

	void LoadEndingSceneTitl()
    {
		SceneManager.LoadScene("TitlScene");
	}






	public void StageSelect()
	{
		switch (SelectNumber)
		{
			//case 0:
			//	SelectNumber = 20;
			//	Debug.Log("case0");
			//	StageSelect();
			//	break;

			case 0:
				//ステージ1の情報を表示
				Debug.Log("Stage1");
				break;

			case 1:
				//ステージ2の情報を表示
				Debug.Log("Stage2");
				break;

			case 2:
				//ステージ3の情報を表示
				Debug.Log("Stage3");
				break;

			case 3:
				//ステージ4の情報を表示
				Debug.Log("Stage4");
				break;

			case 4:
				//ステージ5の情報を表示
				Debug.Log("Stage5");
				break;

			case 5:
				//ステージ6の情報を表示
				Debug.Log("Stage6");
				break;

			case 6:
				//ステージ7の情報を表示
				Debug.Log("Stage7");
				break;

			case 7:
				//ステージ8の情報を表示
				Debug.Log("Stage8");
				break;

			case 8:
				//ステージ9の情報を表示
				Debug.Log("Stage9");
				break;

			case 9:
				//ステージ10の情報を表示
				Debug.Log("Stage10");
				break;

			case 10:
				//ステージ11の情報を表示
				Debug.Log("Stage11");
				break;

			case 11:
				//ステージ12の情報を表示
				Debug.Log("Stage12");
				break;

			case 12:
				//ステージ13の情報を表示
				Debug.Log("Stage13");
				break;

			case 13:
				//ステージ14の情報を表示
				Debug.Log("Stage14");
				break;

			case 14:
				//ステージ15の情報を表示
				Debug.Log("Stage15");
				break;

			case 15:
				//ステージ16の情報を表示
				Debug.Log("Stage16");
				break;

			case 16:
				//ステージ17の情報を表示
				Debug.Log("Stage17");
				break;

			case 17:
				//ステージ18の情報を表示
				Debug.Log("Stage18");
				break;

			case 18:
				//ステージ19の情報を表示
				Debug.Log("Stage19");
				break;

			case 19:
				//ステージ20の情報を表示
				Debug.Log("Stage20");
				break;

		}
	}
}
