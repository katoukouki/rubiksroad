using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectQuad : MonoBehaviour
{
    public bool isChecked = false;

    //GameObject cubeManager;
    //GameObject cameraMove;

    public GameObject startPrefab;
    public GameObject goalPrefab;
    public GameObject JorkerPrefab;


    public enum QuadColor
    {
        NONE,
        GREEN,
        RED,
        BLUE,
        YELLOW,
        ORENGE,
        WHITE,
        JOKER
    };
    public QuadColor quadColor;

    Color[] col =
    {
        Color.black,    //これは使わない
        new Color(0 , 1, 0.602f), //緑//Color.green,
        Color.red,
        Color.blue,
        Color.yellow,
        new Color(1 , 0.5f, 0),
        Color.white,
        //new Color(0 , 1, 0.602f), //緑
        //new Color(0.9803f , 0.0941f, 0), //赤
        //new Color(0.0196f , 0.5960f, 0.9843f), //青
        //new Color(0.9333f , 0.7568f, 0), //黄色
        //new Color(0.9098f , 0.5372f, 0.4352f), //オレンジ
        //new Color(0.8392f , 0.9568f, 0.9803f), //白
    };

    //public GameObject checkSphere;

    public bool isStart = false;
    public bool isGoal = false;



    void Start()
    {
        //cubeManager = GameObject.Find("CubeManager");
        //cameraMove = GameObject.Find("Center");


        if (isStart)
        {
            //cubeManager.GetComponent<CubeManager>().StartQuad.Add(this.gameObject);
            GameObject go = Instantiate(startPrefab, transform.position + transform.forward * -0.02f, transform.rotation);
            //go.transform.parent = this.transform;
        }

        else if (isGoal)
        {
            //cubeManager.GetComponent<CubeManager>().GoalQuad.Add(this.gameObject);
            GameObject go = Instantiate(goalPrefab, transform.position + transform.forward * -0.02f, transform.rotation);
            //go.transform.parent = this.transform;
        }


        if (quadColor == QuadColor.NONE)
        {
            Destroy(gameObject);
        }
        else
        {
            if (quadColor == QuadColor.JOKER)
            {
                GameObject go = Instantiate(JorkerPrefab, transform.position + transform.forward * -0.005f + transform.up * 0.08f, transform.rotation);
                //go.transform.parent = this.transform;
                GetComponent<Renderer>().material.color = Color.black;
            }
            else
            {
                GetComponent<Renderer>().material.color = col[(int)quadColor];
            }

            for (int i = 0; i < 4; i++)
            {
                transform.GetChild(i).GetComponent<SphereCollider>().radius = 0.08f;
            }

        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
