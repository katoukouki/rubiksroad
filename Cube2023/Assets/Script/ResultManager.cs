using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultManager : MonoBehaviour
{

    public GameObject FeedOut;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Clear()
    {
        //効果音再生 0.3秒後にPlaySEメソッドを呼ぶ
        Invoke("PlaySE", 0);
        Invoke("LoadEndingSceneClear", 1.5f);
        FeedOut.SetActive(true);
    }

    public void GameOver()
    {
        //効果音再生 0.3秒後にPlaySEメソッドを呼ぶ
        Invoke("PlaySE", 0);
        Invoke("LoadEndingSceneGameOver", 1.5f);
        FeedOut.SetActive(true);
    }

    void LoadEndingSceneClear()
    {
        SceneManager.LoadScene("StageSelectScene");
    }

    void LoadEndingSceneGameOver()
    {
        SceneManager.LoadScene("StageSelectScene");
    }

    void PlaySE()
    {
        //効果音再生
        GetComponent<AudioSource>().Play();
    }
}
