using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cube : MonoBehaviour
{
    GameObject manager; //マネージャー


    // Start is called before the first frame update
    void Start()
    {
        manager = GameObject.Find("CubeManager");　//CubeManagerを変数に入れる
        ArraySet();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //マネージャーの3次元配列の正しい位置に自分を入れる
    public void ArraySet()
    {

        //現在の位置を四捨五入
        int x = (int)(transform.position.x + 0.5f);
        int y = (int)(transform.position.y + 0.5f);
        int z = (int)(transform.position.z + 0.5f);

        transform.position = new Vector3(x, y, z); //位置調整




        //配列に入れる
        manager.GetComponent<CubeManager>().cube[x, y, z] = this.gameObject;

        //角度調整
        x = (int)(transform.localEulerAngles.x / 90 + 0.5f) * 90;
        y = (int)(transform.localEulerAngles.y / 90 + 0.5f) * 90;
        z = (int)(transform.localEulerAngles.z / 90 + 0.5f) * 90;
        transform.localEulerAngles = new Vector3(x, y, z);
        //Debug.Log(transform.localEulerAngles);
    }
}
